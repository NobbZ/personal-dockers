DOTZSHDIR=~/.zsh

# prepare dots expansion
expand-or-complete-with-dots() {
  echo -n "\e[31m...\e[0m"
  zle expand-or-complete
  zle redisplay
}
zle -N expand-or-complete-with-dots
bindkey "^I" expand-or-complete-with-dots

# Load completions
fpath=(${DOTZSHDIR}/compdefs $fpath)
autoload -U compinit && compinit

# Set up prompt
autoload -U promptinit && promptinit
prompt adam2
