#!/usr/bin/env bash

set -ex

docker build -t nobbz/base:latest base
docker build -t nobbz/erlang:latest erlang
docker build -t nobbz/elixir:latest elixir
docker build -t nobbz/node_over_ex:latest node_over_ex
docker build -t nobbz/phoenix:latest phoenix
docker build -t nobbz/phoenix_with_buildtools:latest phoenix_with_buildtools
