#!/usr/bin/env bash

set -ex

./build.sh

docker push nobbz/base:latest
docker push nobbz/erlang:latest
docker push nobbz/elixir:latest
docker push nobbz/node_over_ex:latest
docker push nobbz/phoenix:latest
docker push nobbz/phoenix_with_buildtools:latest
